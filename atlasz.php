<?php

require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
require_once(getenv('OB_LIB_DIR').'base_functions.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.");

$res = pg_query($ID, "SELECT DISTINCT species FROM openherpmaps_atlas");
$species_list = pg_fetch_all($res);


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Atlas of the Romanian Herpetofauna - OpenHerpMaps</title>
        <meta name="description" content="Live atlas viewer of the OpenHerpMaps database." />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Bootstrap 4-->
        <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/private/bootstrap.min.css?rev=<?php echo rev('css/private/bootstrap.min.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/private/custom.css?rev=<?php echo rev('css/private/custom.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet/leaflet.css?rev=<?php echo rev('/vendor/leaflet/leaflet.css') ?>" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <!--icons-->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    </head>
    <body>
        <!--header-->
        <nav class="navbar navbar-expand-md navbar-dark">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ion-grid icon-sm"></span>
            </button>
            <a class="navbar-brand hero-heading float-right" href="#"><img src="<?= $protocol. "://" . URL ?>/images/logo.png" alt="OpenHerpMaps logo"></a>
            <div class="collapse navbar-collapse" id="navbarCollapse">
<!--
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mr-3">
                        <a class="" href="">Alege specia <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
-->
            </div>
        </nav>

        <!-- map -->
        <section>
            <div id="atlas-map"> </div> 
            <div class="side-nav">
                <a class="btn select-species">
                   <span class="ion-chevron-left icon-sm"></span>
                </a> 
                <a class="btn">
                   <span class="ion-social-facebook icon-sm"></span>
                </a> 
            </div>
            <div class="species-list"> 
                <ul>
                    <?php foreach ($species_list as $sp) {
                        $species = $sp['species'];
                        $image_name = strtolower(str_replace(' ','_',$species));
                    ?>
                        <a href="" id="<?= $image_name ?>" class="species-button">
                            <li class="btn">
                    <?php if (file_exists( getenv('PROJECT_DIR').'images/'.$image_name.'.png')) : ?>
                                <img src="<?php echo $protocol ?>://<?php echo URL ?>/images/hyla_arborea.jpg" alt="Hyla arborea" style="max-width: 50px;"/>
                    <?php endif; ?>
                                <em><?= $species ?></em>
                            </li>
                        </a> 
                    <?php  } //endforeach; ?>
                </ul> 
            </div> 

        </section> 
        


        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
        <script src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet/leaflet.js?rev=<?php echo rev('vendor/leaflet/leaflet.js'); ?>"> </script>
        <script src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet/proj4.js?rev=<?php echo rev('vendor/leaflet/proj4.js'); ?>"> </script>
        <script src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet/proj4leaflet.js?rev=<?php echo rev('vendor/leaflet/proj4leaflet.js'); ?>"> </script>
        <script src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet/leaflet.wms.js?rev=<?php echo rev('vendor/leaflet/leaflet.wms.js'); ?>"> </script>
        <script src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet/leaflet-wfst.min.js?rev=<?php echo rev('vendor/leaflet/leaflet-wfst.min.js'); ?>"> </script>
        <script src="<?php echo $protocol ?>://<?php echo URL ?>/js/private/scripts.js?rev=<?php echo rev('js/private/scripts.js'); ?>"></script>
    <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/openherpmaps/js/functions.js?rev=1558113490"></script>
    <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/openherpmaps/js/roller.js?rev=1558113490"></script>
    <!-- <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/openherpmaps/js/main.js?rev=1558113490"></script> -->
    <!-- <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/openherpmaps/includes/modules.js.php?rev=1558116639"></script> -->
    </body>
</html>
