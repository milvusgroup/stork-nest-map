/* 
   Created on : Jul 4, 2017, 12:43:10 AM
   Author     : Atta-Ur-Rehman Shah (http://attacomsian.com)
   */


$(document).ready (function () {
    var map = new L.Map('atlas-map', {
    });

    L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', { //https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {
        foo: 'bar', 
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
        }).addTo(map);
    map.setView([46.505, 25.09], 7)


    let options = {
        map: 'PMAP',
        isBaseLayer: 'false',
        format: 'image/png',
        visibility: 'true',
        opacity: 1.0,
        transparent: true,
        numzoomlevels: 20,
        maxZoom: 18,
        minZoom: 6,
        attribution: 'Romanian Herp Atlas',
        layers: "layer_data_atlas",
        cname: "layer_data_atlas",
    };

    var overlay = L.WMS.overlay("http://milvus.openbiomaps.org/projects/openherpmaps/private/proxy-atlas.php", options);

    overlay.addTo(map);

    const speciesButtons = document.getElementsByClassName('species-button');
    for (let i = 0; i < speciesButtons.length; i++) {

        const speciesBtn = speciesButtons.item(i);
        L.DomEvent.on( speciesBtn, 'click', function(ev) {
            ev.preventDefault();
            const sp = speciesBtn.getElementsByTagName('em')[0].innerHTML;
            if (sp) {
                options.species = sp;
                map.removeLayer(overlay);
                overlay = L.WMS.overlay("http://milvus.openbiomaps.org/projects/openherpmaps/private/proxy-atlas.php", options);
                overlay.addTo(map);
            }
        });
    }
});


$(document).ready(function() {
    $('.select-species').on('click', function(ev) {
        ev.preventDefault();
        const w = 400;
        if ($(".species-list").is(':visible')) {
            $(this).parent().animate({'right':'0'});
        }
        else {
            $(this).parent().animate({'right':(w-40)+'px'});
        }
        $(".species-list").css('width',w).animate({width:'toggle'},w);
        $(this).find('span').toggleClass('ion-chevron-left').toggleClass('ion-chevron-right');
    });
});


/*
//init 
init();
//init wow effects
new WOW().init();

//scroll menu
$(window).scroll(function () {
init();
});

//page scroll
$('a.page-scroll').bind('click', function (event) {
var $anchor = $(this);
$('html, body').stop().animate({
scrollTop: $($anchor.attr('href')).offset().top - 50
}, 1500, 'easeInOutExpo');
event.preventDefault();
});

//init function
function init() {
var secondFeature = $('#features').offset().top;
var scroll = $(window).scrollTop();
if (scroll >= 150) {
$('.sticky-navigation').css({"background-color": '#d81b60'});
} else {
$('.sticky-navigation').css({"background-color": 'transparent'});
}
if (scroll >= secondFeature - 200) {
$(".mobileScreen").css({'background-position': 'center top'});
}
return false;
}
*/
